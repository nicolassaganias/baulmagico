# baulMágico

sistema de apertura, maquina de humo, sonido y luces dentro de un baúl. alimentado por 2 baterías, una de gel de 12v y un powerbank de 5v. todo controlado con un mando a distancia por radiofrecuencia.

CONEXIONES:
_importantisimo! todas las GND van conectadas entre si!_

conexiones mosfet arduino: [mosfet arduino](https://bildr.org/2012/03/rfp30n06le-arduino/)
conexiones dfplayer arduino nano [dfplayer mini](https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299)

para hacer la antena del RF433 seguí este tutorial: [antena rf433](https://www.youtube.com/watch?v=T4cS-uP_dKM)
RF433         ARDUINO NANO
- GND     -----   GND
- VCC     -----   5v
- RX      -----   2
- PIN LIBRE

MOSFET1       ARDUINO NANO       
- G       -----   pin 6 y GND   
- D       -----   
- S       -----

MOSFET2       ARDUINO NANO        
- G       -----   pin 7 y GND   
- D       -----   
- S       -----

RELAY         ARDUINO NANO        
- VCC     -----   5v
- GND     -----   GND
- DAT     -----   9

LEDs          ARDUINO NANO        
- VCC     -----   5v
- GND     -----   GND
- DAT     -----   8

DFPlayer      ARDUINO NANO        
- VCC    -----   5v
- RX     -----   11
- TX     -----   10
- GND    -----   GND   -------- stereo jack sleeve
- DACR   ---------------------- stereo jack tip
- DACL   ---------------------- stereo jack ring


