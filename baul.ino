#include <RCSwitch.h>

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

#include <Adafruit_NeoPixel.h>

#define PIN 8
int NUMPIXELS = 74;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

RCSwitch mySwitch = RCSwitch();
long controlValue;
int estado = 0;

unsigned long previousMillis = 0;

const int smokeMachine = 9;

const long smokeInterval = 3000;
int ledState = LOW;

const int baul = 6;
const long baulInterval = 1000;

const int baul2 = 7;
const long baul2Interval = 1000;


void setup() {

  delay(2000);
  
  mySoftwareSerial.begin(9600);

  Serial.begin(115200);

  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while (true) {
      delay(0); // Code to compatible with ESP8266 watch dog.
    }
  }
  Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(20);  //Set volume value. From 0 to 30

  pinMode(baul, OUTPUT);
  pinMode(baul2, OUTPUT);
  pinMode(smokeMachine, OUTPUT);
  digitalWrite(baul, LOW);
  digitalWrite(baul2, LOW);
  digitalWrite(smokeMachine, HIGH);

  mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2

  pixels.begin();
  pixels.clear();
  pixels.show();

}

void loop() {
  if (mySwitch.available()) {

    // Serial.print("Received ");
    //Serial.println( mySwitch.getReceivedValue() );

    if (mySwitch.getReceivedValue() == 3667297) { // BOTON 1
      delay(50);

      playAudio1();

    }
    else if (mySwitch.getReceivedValue() == 3667298) { // BOTON 2

      delay(50);
      startSmoke();
      startLEDs();
      playAudio2();
    }
    else if (mySwitch.getReceivedValue() == 3667304) { // BOTON 3
      delay(50);
      stopLEDs();
      openBaul();
      startSmoke();
      playAudio3();

    }
    else if (mySwitch.getReceivedValue() == 3667300) { // BOTON 4
      delay(50);

      playAudio4();
      openBaul2();

    }

    mySwitch.resetAvailable();
  }
}

void startSmoke() {
  Serial.println("Start Smoke Machine");

  digitalWrite(smokeMachine, LOW);
  delay(smokeInterval);
  digitalWrite(smokeMachine, HIGH);

}

void openBaul() {
  Serial.println("Open Baúl");

  digitalWrite(baul, HIGH);
  delay(baulInterval);
  digitalWrite(baul, LOW);
}

void openBaul2() {
  Serial.println("Open Baúl");

  digitalWrite(baul2, HIGH);
  delay(baul2Interval);
  digitalWrite(baul2, LOW);
}

void startLEDs() {
  Serial.println("LEDs");

  uint32_t magenta = pixels.Color(255, 0, 255);

  pixels.fill(magenta);
  pixels.show();

}

void stopLEDs() {
  Serial.println("stop LEDs");

  pixels.clear();
  pixels.show();

}

void playAudio1() {

  Serial.println("Start Sound 1");

  myDFPlayer.stop();

  delay(100);

  myDFPlayer.play(1);
}

void playAudio2() {

  Serial.println("Start Sound 2");

  myDFPlayer.stop();

  delay(100);

  myDFPlayer.play(2);
}

void playAudio3() {

  Serial.println("Start Sound 3");

  myDFPlayer.stop();

  delay(100);

  myDFPlayer.play(3);
}

void playAudio4() {

  Serial.println("Start Sound 4");

  myDFPlayer.stop();

  delay(100);

  myDFPlayer.play(4);
}
